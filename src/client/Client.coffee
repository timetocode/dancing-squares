# client

config = require './config'
Player = require '../shared/player'

class Client
  constructor: () ->
    @connected = false
    @connection = new WebSocket config.getWebsocketAddress()
    @gameState = null
    @fullUpdateAction = null
    @engine = null
    @

    @connection.onopen = () =>
      @connected = true
      msg = { cmd: 'login', username: 'newPlayer' }
      @sendMessage msg

    @connection.onerror = (error) ->
      alert 'connection error'

    @connection.onmessage = (message) =>
      try
        json = JSON.parse message.data
        @readMessage json
      catch e
        console.log "JSON.parse error: ", e.message
        console.log message.data
        return
    @

  readMessage: (json) =>
    switch json.msg
      when 'player'
        console.log json.data
        if @gameState.players[json.data.id]
          # TODO REFACTOR
          console.log 'player update'              
          player = @gameState.players[json.data.id]
          # for other players
          if player isnt @gameState.player
            # create a shallow copy of the player for our records
            # save it as the 'PreviousState' so we can use it for 
            # entity interpolation later
            copyPlayer = new Player()
            copyPlayer.x = player.x
            copyPlayer.y = player.y
            copyPlayer.lastUpdate = player.lastUpdate
            @gameState.playersPreviousState[json.data.id] = copyPlayer
            
            # update our player's state
            player.id = json.data.id
            player.name = json.data.name
            player.color = json.data.color
            player.speed = json.data.speed
            player.mx = json.data.mx
            player.my = json.data.my
            player.x = json.data.x
            player.y = json.data.y
            player.snapshot = json.snapshot
            player.chat = json.data.chat
            player.chatTime = json.data.chatTime
            player.lastUpdate = Date.now()
          # for our own player
          else
            copyPlayer = new Player()
            copyPlayer.x = json.data.x
            copyPlayer.y = json.data.y
            copyPlayer.lastUpdate = Date.now()

            @gameState.authoritativeState = copyPlayer
            #player.x = json.data.x
            #player.y = json.data.y
            # all data minus movement stuff
            player.id = json.data.id
            player.name = json.data.name
            player.color = json.data.color
            player.speed = json.data.speed
            player.snapshot = json.snapshot
            player.chat = json.data.chat
            player.chatTime = json.data.chatTime
        else
          # first time we've heard bout a player
          console.log 'new player'
          player = new Player()
          player.id = json.data.id
          player.name = json.data.name
          player.color = json.data.color
          player.speed = json.data.speed
          player.mx = json.data.mx
          player.my = json.data.my
          player.x = json.data.x
          player.y = json.data.y
          
          @gameState.players[player.id] = player
          @gameState.playersPreviousState[player.id] = null
      when 'identity'
        console.log 'Received Identity'
        @gameState.player = @gameState.players[json.id]
        console.log "I have been identified as #{@gameState.player.name}(#{@gameState.player.id})"
      when 'tickrate'
        console.log 'Received Tickrate'
        @engine.UPDATE_TICKRATE = json.tickrate


  sendMessage: (messageObject) =>
    if @connected
         #console.log 'sending ' + JSON.stringify messageObject
        @connection.send JSON.stringify messageObject

module.exports = Client
