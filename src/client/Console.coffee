# developer console

class Console
  constructor: (inputElement, displayElement) ->
    @inputEle = inputElement    # the console's Prompt (an html input)
    @dispEle = displayElement    # the console's output (a div, span, etc) 
    @submitAction = null    # function to run on the input
    @enabled = false  
    @commands = [] # a log of commands issued to the console
    @pos = 0 # the position of the current command, used for scrolling through the log

  enable: () ->
    # ensures html elements have been specified
    # ensures that the console is not already active
    if @inputEle && @dispEle && !@enabled
      @enabled = true
      @captureInput() # capture keystrokes

  disable: () ->
    @enabled = false

    if @inputEle
      @inputEle.unbind 'keydown.console' # unbind only the '.console' namespace
      #@inputEle = null
    #@dispEle = null if @dispEle


  captureInput: () ->
    # '.console' is a namespace, so that we can unbind just this console handler later
    @inputEle.bind 'keydown.console', (e) =>
      if e.keyCode is 38 # UP ARROW
        # scroll 'up' aka backwards through previous console commands
        if @pos < @commands.length
          @pos++
          @inputEle.val(@commands[@commands.length - @pos])

      if e.keyCode is 40 # DOWN ARROW
        # scroll 'down' aka forwards through previous console commands
        if @pos > 0
          @pos--       
          @inputEle.val(@commands[@commands.length - @pos])          
        else   
          @inputEle.val('') # clear the console prompt

      if e.keyCode is 13 # ENTER
        if @inputEle.val() isnt ''
          submitText = @inputEle.val()
          # remember this new command
          @commands.push submitText

          # run the 'subtmitAction' on the text and display the return value or error
          try
            @dispEle.html @submitAction submitText
          catch e
            @dispEle.html(e)

        @inputEle.val ''

module.exports = Console