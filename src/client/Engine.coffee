# engine
Player = require '../shared/player'
math = require '../shared/math'

class Engine
  constructor: (canvas, context) ->
    @gameState = null
    @UPDATE_TICKRATE = 5
    @DRAW_TICKRATE = 60
    @previousUpdateTick = Date.now()
    @previousDrawTick = Date.now()
    @simulationStep = 0
    @drawStep = 0
    @canvas = canvas
    @context = context
    @sendUpdateToServerAction = null
    @userCommands = []
    @ui = null
    @entityInterpolation = true
    @clientsidePrediction = true
    @updateDelta = 0 # updateDelta changes every update, but is used by the draw code for client prediction
    @blendtime = 0
    @

  draw: (delta, drawStep) ->
    @canvas.width = @canvas.width

    # TODO: refactor
    if @gameState.player
      # draw every player except our own
      for id of @gameState.players      
        if parseInt(id) isnt @gameState.player.id # or own player
          # if we have multiple frames of data, interpolate!
          if @entityInterpolation and @gameState.playersPreviousState[id]
            # use Entity interpolation
            @gameState.players[id].drawEntityInterpolation(@context, @gameState.playersPreviousState[id], drawStep, @UPDATE_TICKRATE)
          else
            # otherwise just draw the single frame of data we have
            @gameState.players[id].draw(@context) #ugly code        
      
      # draw our own player using clientside prediction
      if @clientsidePrediction
        @gameState.player.drawClientPrediction(@context, @gameState.playerPreviousState, @UPDATE_TICKRATE, delta, @previousUpdateTick, @updateDelta)
      else
        @gameState.player.draw(@context)  

  update: (delta) ->
    # engine handles player updates itself for now
    # TODO: move this code to player
    if @gameState.player
      player = @gameState.player
      if player.x isnt player.mx or player.y isnt player.my
        dx = player.x - player.mx
        dy = player.y - player.my
        nv = math.normalizeVector dx, dy
        player.x = player.x - nv.x * player.speed * delta
        player.y = player.y - nv.y * player.speed * delta
        if (nv.x < 0) and (player.x > player.mx)
          player.x = player.mx
        if (nv.x > 0) and (player.x < player.mx)
          player.x = player.mx
        if (nv.y < 0) and (player.y > player.my)
          player.y = player.my
        if (nv.y > 0) and (player.y < player.my)
          player.y = player.my

      if @gameState.authoritativeState
        serverPlayer = @gameState.authoritativeState
        if player.x isnt serverPlayer.x or player.x isnt serverPlayer.y
          player.x = math.lerp player.x, serverPlayer.x, 0.1
          player.y = math.lerp player.y, serverPlayer.y, 0.1

      player.lastUpdate = Date.now()

  gameLoop: () =>
    now = Date.now()
    # the update loop
    if @previousUpdateTick + 1000 / @UPDATE_TICKRATE < now
      @updateDelta = (now - @previousUpdateTick) / 1000
      @previousUpdateTick = now
      console.log '>>> ' + @updateDelta
      @simulationStep++
      @update @updateDelta

    now = Date.now()
    # the draw loop
    if @previousDrawTick + 1000 / @DRAW_TICKRATE < now
      drawDelta = (now - @previousDrawTick) / 1000
      @previousDrawTick = now
      @drawStep++
      @draw drawDelta, @drawStep

    window.requestAnimFrame @gameLoop

  begin: () ->
    if @gameState
      console.log 'engine begin'
      window.requestAnimFrame @gameLoop
    else
      alert 'engine cannot begin until it has a gameState'

module.exports = Engine