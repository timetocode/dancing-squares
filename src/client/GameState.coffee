# game state

class GameState 
  constructor: () ->
    @isConnected = false
    @player = null
    @authoritativeState = null
    @players = {}
    @playersPreviousState = {}

module.exports = GameState