# interface

class UI
  constructor: () ->
    @mx = 0
    @my = 0
    @mouseMoveAction = null
    @mouseDownAction = null
    @mouseUpAction = null
    @userCommands = []
    @

  beginCapture: (canvas) ->
    canvas.addEventListener 'mousemove', (e) =>
      #console.log 'aa'
      @mx = e.pageX - canvas.offsetLeft
      @my = e.pageY - canvas.offsetTop
      @mouseMoveAction @mx, @my

    canvas.addEventListener 'mousedown', (e) =>
      @mouseDownAction()

    canvas.addEventListener 'mouseup', (e) =>
      @mouseUpAction()

module.exports = UI