# console parser

# parse console input and returns a formatted command to send to server
# e.g.    /say hi         ->    { cmd : 'say',   text : 'hi' }
# e.g.    /name neuron    ->    { cmd : 'name',  name : 'neuron' }
# e.g.    /move 150, 300  ->    { cmd : 'move',  x : 150, y : 300 }
engine = null
lagSimulator = null

parse = (commandText) ->
  # if it doesnt start with '/' assume they're trying to chat
  if commandText.indexOf('/') isnt 0
    commandText = '/say ' + commandText
  match = commandText.match /\/([a-z_]+) (.+)/

  command = ''
  args = ''

  if match isnt null
    command = match[1]
    args = match[2]

  switch command
    # Client commands
    when 'cl_interp'
      val = parseInt args
      engine.entityInterpolation = (val is 1) ? false : true
      return "Entity Interpolation is now #{engine.entityInterpolation}"
    when 'cl_pred'
      val = parseInt args
      engine.clientsidePrediction = (val is 1) ? false : true
      return "Client Prediction is now #{engine.clientsidePrediction}"
    when 'cl_fps'
      return "Client control of fps/tickrate deprecated. Try /sv_tickrate instead."
    when 'cl_latency'
      lagSimulator.latency = parseInt args
      return "Simulated latency is now #{lagSimulator.latency-lagSimulator.latencyVariance}-#{lagSimulator.latency+lagSimulator.latencyVariance}ms"
    when 'cl_latencyvariance'
      lagSimulator.latencyVariance = parseInt args
      return "Simulated latency is now #{lagSimulator.latency-lagSimulator.latencyVariance}-#{lagSimulator.latency+lagSimulator.latencyVariance}ms"
    when 'sv_tickrate'
      return { cmd : command, data : args }
    when 'playerspeed'
      return { cmd : command, data : args }
    when 'say'
      return { cmd : command, data : args }
    when 'name'
      return { cmd : command, name : args }
    else
      throw 'Unknown command: "' + commandText + '"'

send = null

exports.bindSend = (fn) ->
  send = fn

exports.setEngine = (eng) ->
  engine = eng

exports.setLagSimulator = (lagSim) ->
  lagSimulator = lagSim

exports.processConsoleInput = (consoleText) ->
  command = parse consoleText
  send command
  return command