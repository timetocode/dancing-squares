# main

Console = require './Console'
consoleParser = require './consoleParser'
LagSimulator = require '../shared/LagSimulator'

testConsole = new Console($('#input'), $('#debug'))
testConsole.submitAction = consoleParser.processConsoleInput
testConsole.enable()

GameState = require './gameState'

Engine = require './engine'
UI = require './ui'
Client = require './Client'
client = new Client()

consoleParser.bindSend client.sendMessage

# use a browser-specific version of requestAnimationFrame
window.requestAnimFrame = (->
  window.requestAnimationFrame or window.webkitRequestAnimationFrame or window.mozRequestAnimationFrame or (callback) ->
    window.setTimeout callback, 1000 / 60
)()

# use a browser-specific version of websockets
window.WebSocket = window.WebSocket or window.MozWebSocket

canvas = document.getElementById("myCanvas")
context = canvas.getContext("2d")

lagSimulator = new LagSimulator()
lagSimulator.setLatency = 0
lagSimulator.setLatencyVairance = 0
ui = new UI()

gameState = new GameState()
client.gameState = gameState
engine = new Engine(canvas, context)
client.engine = engine
engine.gameState = gameState
engine.ui = ui
consoleParser.setEngine engine
consoleParser.setLagSimulator lagSimulator

isMouseDown = false
client.fullUpdateAction = (snapshot) ->
  engine.simulationStep = snapshot

ui.mouseMoveAction = (x, y) ->
  if isMouseDown
    ui.mx = x
    ui.my = y

    lagSimulator.withLatency () =>
      client.sendMessage { cmd : 'm', x: ui.mx, y : ui.my }

    if gameState.player
      gameState.player.action = 'moving'
      gameState.player.mx = ui.mx
      gameState.player.my = ui.my

ui.mouseDownAction = () ->
  isMouseDown = true

  lagSimulator.withLatency () =>
    client.sendMessage { cmd : 'm', x: ui.mx, y : ui.my }

  if gameState.player
      gameState.player.action = 'moving'
      gameState.player.mx = ui.mx
      gameState.player.my = ui.my
  console.log engine.simulationStep

ui.mouseUpAction = () ->
  isMouseDown = false

  lagSimulator.withLatency () =>
    client.sendMessage { cmd : 'i' }

  if gameState.player
    gameState.player.action = 'idle'

ui.beginCapture(canvas)

engine.begin()



