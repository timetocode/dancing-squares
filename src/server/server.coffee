# server
webSocketServer = require('websocket').server
http = require 'http'
st = require 'node-static'
url = require 'url'
fileServer = new st.Server './'
math = require '../shared/math'
Player = require '../shared/Player'

webSocketServerPort = 8080
TICKS_PER_SECOND = 5

previousTime = Date.now()
previousTick = Date.now()
snapshot = 0

# list of currently connected clients (users)
clients = []
players = []

colors = ["red", "green", "blue", "magenta", "purple", "plum", "orange"]
# ... in random order
colors.sort (a, b) ->
  Math.random() > 0.5


generateClientId = () ->
  id = 0
  if clients
    id = clients.length
  return id

gameServer = {}

# TODO: use the player class for this stuff
# instead of hardcoding into a few server functions
gameServer.createPlayer = (id, name) ->
  player = new Player()
    # position
  player.x = 100
  player.y = 100
  # mouse position
  player.mx = 100
  player.my = 100
  player.id = id
  player.name = 'player'
  player.speed = 165
  player.status = 'idle'
  player.color = colors.shift()
  player.name = name
  return player

gameServer.sendPlayerUpdates = () ->
  for client in clients
    for player in players
      playerData = JSON.stringify { msg: 'player', snapshot : snapshot, data : player }
      client.sendUTF playerData

server = http.createServer (req, res) ->
  req.addListener 'end', () ->
    _get = url.parse(req.url, true).query
    fileServer.serve req, res

server.listen webSocketServerPort, () ->
  console.log new Date() + ' Server is listening on port ' + webSocketServerPort

wsServer = new webSocketServer({ httpServer : server})

wsServer.on "request", (request) ->
  console.log (new Date()) + " Connection from origin " + request.origin + "."  
  connection = request.accept(null, request.origin)  
  console.log (new Date()) + " Connection accepted."

  id = generateClientId()
  console.log "GENERATED ID: #{id}"
  clients[id] = connection

  # per connection variables
  player = false

  
  # user sent some message
  connection.on 'message', (message) ->
    #console.log message
    if message.type is 'utf8' # accept only text     
      obj = JSON.parse message.utf8Data
      #console.log obj

      switch obj.cmd
        when 'm'
          player.mx = obj.x
          player.my = obj.y
          player.action = 'moving'
          #console.log 'movement'
        when 'i'
          player.action = 'idle'
          #console.log 'stopped movement'
        when 'login'    # username
          console.log "#{obj.username} logged in"
          players[id] = gameServer.createPlayer(id, obj.username)
          player = players[id]
          gameServer.sendPlayerUpdates()
          clients[id].sendUTF JSON.stringify { msg : 'identity', id : id }
        when 'say'      # text
          console.log obj.data
          players[id].chat = obj.data
          players[id].chatTime = Date.now()
        when 'name'     # name
          player.name = obj.name
          console.log "Renamed to #{obj.name}"
        when 'sv_tickrate'
          console.log "Server FPS changed to #{obj.data}"
          TICKS_PER_SECOND = parseFloat obj.data
          clients[id].sendUTF JSON.stringify { msg : 'tickrate', tickrate : TICKS_PER_SECOND }
        when 'playerspeed'
          players[id].speed = obj.data
  
  # user disconnected
  connection.on "close", (connection) ->
    console.log (new Date()) + " Peer " + connection.remoteAddress + " disconnected."      
    # remove user from the list of connected clients
    clients.splice clients.indexOf id, 1


update = (delta) ->
  # TODO: put this code into a shared class
  # so that identical movement code may be run
  # on both server and client, with no code dupelication
  for player in players
    if player.x isnt player.mx or player.y isnt player.my
      dx = player.x - player.mx
      dy = player.y - player.my
      nv = math.normalizeVector dx, dy
      player.x = player.x - nv.x * player.speed * delta
      player.y = player.y - nv.y * player.speed * delta
      if (nv.x < 0) and (player.x > player.mx)
        player.x = player.mx
      if (nv.x > 0) and (player.x < player.mx)
        player.x = player.mx
      if (nv.y < 0) and (player.y > player.my)
        player.y = player.my
      if (nv.y > 0) and (player.y < player.my)
        player.y = player.my

gameLoop = () ->
  now = Date.now()
  if now - previousTime > 1
    console.log "lagged by #{now - previousTime} ms"
  if previousTick + 1000 / TICKS_PER_SECOND < now
    delta = (now - previousTick) / 1000
    previousTick = now
    snapshot++
    console.log 'snapshot: ' + snapshot + ' @ ' + now + ' d ' + delta
    update(delta)
    gameServer.sendPlayerUpdates()

  previousTime = now
  process.nextTick gameLoop # releases control until nextTick of node

gameLoop()