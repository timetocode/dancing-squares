# latency simulator
math = require './math'

# use withLatency to simulate high pings
class LagSimulator
  constructor: ->
    # delay, in milliseconds e.g. 150 ms
    @latency          = 0
    # broadens the delay, eg 100 ms w/ variance 20 is 80-120ms
    @latencyVariance  = 0

  setLatency: (ms) ->
    @latency = ms

  setLatencyVariance: (ms) ->
    @latencyVariance = ms

  # applies a delay to a function
  withLatency: (fn) ->
    delay = math.randomInt @latency - @latencyVariance, @latency + @latencyVariance
    console.log delay + 'ms'
    setTimeout fn, delay

module.exports = LagSimulator
  