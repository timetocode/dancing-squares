# player
math = require './math'

class Player
  constructor: () ->
    # position
    @x = 0
    @y = 0

    # movement destination
    @mx = 0
    @my = 0

    @id = 0
    @name = 'player'
    @color = 'pink'

    @speed = 200
    @status = 'idle'

    @chat = ''
    @chatTime = null

    @snapshot = 0
    @

  toString: () ->
    "#{@id} : #{@name} [#{@x}, #{@y}] (#{@color})," + 
    " cursor: [#{@mx}, #{@my}], speed: #{@speed}, " +
    " status: #{@action}"

  draw: (ctx, x, y) ->
    x = @x if typeof(x) is 'undefined'
    y = @y if typeof(y) is 'undefined'
    ctx.beginPath()
    ctx.rect x, y, 36, 36
    ctx.fillStyle = @color
    ctx.fill()
    ctx.lineWidth = 1
    ctx.strokeStyle = "black"
    ctx.stroke()
    ctx.font = 'bold 12px sans-serif'
    ctx.fillText "#{@name} (#{@id})", x - 1, y - 3

    now = Date.now()
    if  now - 5000 < @chatTime
      ctx.fillStyle = 'black'
      ctx.fillText @chat, x - 1, y - 13

  # need a new approach to clientpredictin
  drawClientPrediction: (ctx, previousState, updateTickRate, delta, previousUpdateTick, updateDelta) ->
    now = Date.now()
    player = @

    dx = player.x - player.mx
    dy = player.y - player.my

    # if player isnt moving, skip the rest and just draw
    if dx + dy is 0
      return @draw ctx, @x, @y

    # get the raw direction of movement
    nv = math.normalizeVector dx, dy

    # if our speed would carry us past our movement target
    # then just stop at the target
    if (nv.x < 0) and (player.x > player.mx)
      player.x = player.mx
    if (nv.x > 0) and (player.x < player.mx)
      player.x = player.mx
    if (nv.y < 0) and (player.y > player.my)
      player.y = player.my
    if (nv.y > 0) and (player.y < player.my)
      player.y = player.my

    # predict where we would be next update tick
    simX = player.x - nv.x * player.speed * updateDelta
    simY = player.y - nv.y * player.speed * updateDelta

    # similar to above, make sure our future prediction
    # does not go past our actual movement target
    if (nv.x < 0) and (simX > player.mx)
      simX = player.mx
    if (nv.x > 0) and (simX < player.mx)
      simX = player.mx
    if (nv.y < 0) and (simY > player.my)
      simY = player.my
    if (nv.y > 0) and (simY < player.my)
      simY = player.my

    # interpolate from our current position to our future position
    tickLength = 1000 / updateTickRate
    elapsed = now - player.lastUpdate
    interpX = math.lerp @x, simX, elapsed / tickLength
    interpY = math.lerp @y, simY, elapsed / tickLength
      
    @draw ctx, interpX, interpY 

  drawEntityInterpolation: (ctx, previousState, drawStep, updateTickRate) ->
    now = Date.now()
    tickLength = 1000 / updateTickRate

    elapsed = now - previousState.lastUpdate - tickLength
    interpX = math.lerp previousState.x, @x, elapsed / tickLength
    interpY = math.lerp previousState.y, @y, elapsed / tickLength

    @draw ctx, interpX, interpY

  # not currently in use
  update: (delta) ->
    if @action is 'moving'
      dx = @x - @mx
      dy = @y - @my
      nv = math.normalizeVector dx, dy
      @x = @x - nv.x * @speed * delta
      @y = @y - nv.y * @speed * delta

module.exports = Player