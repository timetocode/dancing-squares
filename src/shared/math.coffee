# math
normalizeVector = (x, y) ->
  length = Math.sqrt (x * x) + (y * y)
  return { 
    x : x / length 
    y : y / length 
  }

lerp = (a, b, amount) ->
  a + ((b - a) * amount)

randomInt = (min, max) ->
  Math.floor(Math.random() * (max - min + 1)) + min

exports.normalizeVector = normalizeVector
exports.lerp = lerp
exports.randomInt = randomInt

